import { Component, OnInit } from '@angular/core';
import{EmployeeService} from '../employee.service';
  import { from } from 'rxjs';

@Component({
  selector: 'app-employeelist',
  templateUrl: './employeelist.component.html',
  styleUrls: ['./employeelist.component.css']
})
export class EmployeelistComponent implements OnInit {
EmpData:any=[];
id:any;

  constructor(private service:EmployeeService) { }

  getEmployee(){
    this.service.getEmp().subscribe(res=>
      this.EmpData=res);
  }

  delete(id){
    this.service.delete(id).subscribe(res=>
    this.getEmployee());
  }
  setIdUpdate(id:any){
    this.id=id;
    console.log(id+"this is"+this.id);
    this.service.setIdUpdate(this.id);

  }

  ngOnInit() {
    this.getEmployee();
  }
  
  
 



}
