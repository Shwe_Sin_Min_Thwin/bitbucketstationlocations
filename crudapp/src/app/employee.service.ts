import { Injectable } from '@angular/core';
import{HttpClient}from '@angular/common/http';
  import { from } from 'rxjs';
import { Player } from '@angular/core/src/render3/interfaces/player';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  idUpdate:any;
  constructor(private http:HttpClient) { }
 
getEmp(){
  return this.http.get("http://dummy.restapiexample.com/api/v1/employees");
}

addPlayer(emp:any){

  return this.http.post("http://dummy.restapiexample.com/api/v1/create",emp);
}

getid(id:any){
  
  return this.http.get("http://dummy.restapiexample.com/api/v1/employee/"+id);

}
update(id:any,employee){
  return this.http.put("http://dummy.restapiexample.com/api/v1/update/"+id,employee);
}

delete(id:any){
  return this.http.delete("http://dummy.restapiexample.com/api/v1/delete/"+id);
}
setIdUpdate(id:any){
  this.idUpdate=id;
  console.log(this.idUpdate);
}
getIdUpdate(){
  return this.idUpdate;
}
}
