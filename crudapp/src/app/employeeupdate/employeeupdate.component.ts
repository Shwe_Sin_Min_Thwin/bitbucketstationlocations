import { Component, OnInit } from '@angular/core';
import { Router, ChildActivationEnd, ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employeeupdate',
  templateUrl: './employeeupdate.component.html',
  styleUrls: ['./employeeupdate.component.css']
})
export class EmployeeupdateComponent implements OnInit {
 
  constructor(private service:EmployeeService,private rou:Router) { }
  id: any;
  data: any=[];
  employee ={"name":"","salary":"","age":""};
  updateEmployee(){
    if(this.employee.name =="")this.employee.name=this.data.employee_name;
    if(this.employee.salary =="")this.employee.salary=this.data.employee_salary;
    if(this.employee.age =="")this.employee.age=this.data.employee_age;
    this.service.update(this.id,this.employee).subscribe(res=> {console.log(res)});
    this.rou.navigate(["/employeelist"]);
  }
  ngOnInit() {
    this.id=this.service.getIdUpdate();
    this.service.getid(this.id).subscribe(res=> this.data=res);
  }


}