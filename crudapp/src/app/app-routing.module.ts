import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { EmployeeupdateComponent } from './employeeupdate/employeeupdate.component';
import { AddemployeeComponent } from './addemployee/addemployee.component';



const routes: Routes = [
  {
    path:"employeelist",component:EmployeelistComponent
  },
  {
    path:"employeelist/:id",component:EmployeeupdateComponent
  },
  {
    path:"add",component:AddemployeeComponent

  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
