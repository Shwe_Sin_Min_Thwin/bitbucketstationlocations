import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeelistComponent } from './employeelist/employeelist.component';
import { EmployeeupdateComponent } from './employeeupdate/employeeupdate.component';
import { AddemployeeComponent } from './addemployee/addemployee.component';
import { HttpClientModule } from '@angular/common/http';
//import { DeleteemployeeComponent } from './deleteemployee/deleteemployee.component';
// import { CommonModule} from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    EmployeelistComponent,
    EmployeeupdateComponent,
    AddemployeeComponent
    
  
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
